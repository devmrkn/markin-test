#!/usr/bin/perl
use strict;
use warnings;
use utf8;

use CGI;
use DBI;
use Data::Dumper;

my $q = new CGI;

my $dbh = DBI->connect(
	'DBI:mysql:db',
	'user',
	'pass',
	{ mysql_enable_utf8 => 1}
) or die $DBI::errstr;

print $q->header('-charset' => 'UTF-8');
print '<form method="POST"><input type="text" name="email" value="' . ($q->param('email') ? $q->param('email') : '') . '"><button type="submit">Найти</button></form>';

if ($ENV{'REQUEST_METHOD'} eq 'POST') {
	my $int_ids = $dbh->selectcol_arrayref(
		'SELECT
			`int_id`
		FROM
			`log`
		WHERE
			`address`=' . $dbh->quote($q->param('email')) . '
		GROUP BY
			`int_id`'
	);

	if (@$int_ids) {
		my $messages = $dbh->selectall_hashref(
			'SELECT
				`int_id`,
				`created`,
				`str`
			FROM
				`message`
			WHERE
				`int_id` IN (' . join(', ', map { '"' . $_ . '"' } @$int_ids) . ')
			ORDER BY
				`created`
			ASC', 1
		);

		my $logs = $dbh->selectall_arrayref(
			'SELECT
				`created`,
				`str`,
				`int_id`
			FROM
				`log`
			WHERE
				`int_id` IN (' . join(', ', map { '"' . $_ . '"' } @$int_ids) . ')
			ORDER BY
				`created`,
				`int_id`
			ASC',
			{ Slice => {} }
		);

		my $count_m = keys %$messages;
		my $count_l = @$logs;

		if (($count_m + $count_l) > 100) {
			print '<p>Число логов превышает 100.</p>';
		} else {
			print '<table border="1"><tr><td>Дата</td><td>Лог</td></tr>';
			foreach my $log (@$logs) {
				my $message = $messages->{ $log->{'int_id'} };
				if ($message) {
					print '<tr><td>' . $message->{'created'} . '</td><td>' . $message->{'str'} . '</td></tr>';
				}
				delete ($messages->{ $log->{'int_id'} });
				print '<tr><td>' . $log->{'created'} . '</td><td>' . $log->{'str'} . '</td></tr>';
			}
			print '</table>';
		}
	} else {
		print '<p>Логи не найдены.</p>';
	}

}

$dbh->disconnect();