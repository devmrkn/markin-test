#!/usr/bin/perl
use strict;
use warnings;

use Data::Dumper;
use DBI;
use Email::Address;

open(my $data, '<', 'data/out');

my (@messages, @logs) = ();

while (my $line = <$data>) {
	chomp $line;

	my @arr = split(' ' , $line);
	my $int_id = $arr[2];

	if ($int_id =~ /(\w{6})-(\w{6})-(\w{2})/) {
		my $timestamp = $arr[0] . ' ' . $arr[1];
		my $is_flag   = 1 if $arr[3] && $arr[3] eq '<=';
		my ($email) = Email::Address->parse($line);
			$email = $email->address if $email;

		$line =~ s/(\d{4})-(\d+)-(\d+)\s(\d+):(\d+):(\d+)\s//;

		if ($is_flag && $email) {
			my $id = pop @arr;
			   $id =~ s/id=//;

			push(@messages, {
				'created' => $timestamp,
				'id'      => $id,
				'int_id'  => $int_id,
				'str'     => $line,
			});
		} else {
			push(@logs, {
				'created' => $timestamp,
				'int_id'  => $int_id,
				'str'     => $line,
				'address' => ($email ? $email : ''),
			});
		}
	}
}

my $dbh = DBI->connect(
	'DBI:mysql:db',
	'user',
	'pass',
	{ mysql_enable_utf8 => 1}
) or die $DBI::errstr;

if (scalar @messages) {
	my $sql_string = join(',', map {
		'(' .
		$dbh->quote($_->{'created'}) . ', ' .
		$dbh->quote($_->{'id'}) . ', ' .
		$dbh->quote($_->{'int_id'}) . ', ' .
		$dbh->quote($_->{'str'}) .
		')'
	} @messages);

	$dbh->do(
		'INSERT INTO
			`message` (`created`, `id`, `int_id`, `str`)
		 VALUES ' . $sql_string
	);
}

if (scalar @logs) {
	my $sql_string = join(',', map {
		'(' .
		$dbh->quote($_->{'created'}) . ', ' .
		$dbh->quote($_->{'int_id'}) . ', ' .
		$dbh->quote($_->{'str'}) . ', ' .
		$dbh->quote($_->{'address'}) . 
		')'
	} @logs);

	$dbh->do(
		'INSERT INTO
			`log` (`created`, `int_id`, `str`, `address`)
		 VALUES ' . $sql_string
	);
}

$dbh->disconnect();

sub _is_email {
	my $str = shift;

	return 1 if $str =~ /^[a-z0-9.]+\@[a-z0-9.-]+$/;
	return;
}